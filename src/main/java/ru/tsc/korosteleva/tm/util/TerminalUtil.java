package ru.tsc.korosteleva.tm.util;

import ru.tsc.korosteleva.tm.exception.field.NumberIncorrectException;

import java.util.Date;
import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() {
        final String value = nextLine();
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            throw new NumberIncorrectException(value);
        }
    }

    static Date nextDate() {
        final String value = nextLine();
        return DateUtil.toDate(value);
    }

}
