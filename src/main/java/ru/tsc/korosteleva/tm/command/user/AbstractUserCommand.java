package ru.tsc.korosteleva.tm.command.user;

import ru.tsc.korosteleva.tm.api.service.IAuthService;
import ru.tsc.korosteleva.tm.api.service.IUserService;
import ru.tsc.korosteleva.tm.command.AbstractCommand;
import ru.tsc.korosteleva.tm.model.User;

import java.util.List;

public abstract class AbstractUserCommand extends AbstractCommand {

    public IUserService getUserService() {
        return serviceLocator.getUserService();
    }

    public IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    protected void showUser(final User user) {
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

    public void renderUsers(final List<User> users) {
        int index = 1;
        for (final User user : users) {
            if (user == null) continue;
            System.out.println(index + ". " + user);
            index++;
        }
    }

}
