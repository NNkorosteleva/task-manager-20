package ru.tsc.korosteleva.tm.repository;

import ru.tsc.korosteleva.tm.api.repository.ITaskRepository;
import ru.tsc.korosteleva.tm.enumerated.Status;
import ru.tsc.korosteleva.tm.model.Task;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    public Task create(final String userId, final String name) {
        final Task task = new Task();
        task.setUserId(userId);
        task.setName(name);
        return add(task);
    }

    @Override
    public Task create(final String userId,
                       final String name,
                       final String description) {
        final Task task = new Task();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        return add(task);
    }

    @Override
    public Task create(final String userId,
                       final String name,
                       final String description,
                       final Date dateBegin,
                       final Date dateEnd) {
        final Task task = new Task();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        task.setDateBegin(dateBegin);
        task.setDateEnd(dateEnd);
        return add(task);
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : records) {
            if (task.getProjectId() == null || task.getUserId() == null) continue;
            if (task.getProjectId().equals(projectId) && task.getUserId().equals(userId)) result.add(task);
        }
        return result;
    }

    @Override
    public Task findOneByName(final String userId, final String name) {
        for (Task task : records) {
            if (task.getUserId() == null) continue;
            if (name.equals(task.getName()) && task.getUserId().equals(userId)) return task;
        }
        return null;
    }

    @Override
    public Task updateById(final String userId,
                           final String id,
                           final String name,
                           final String description) {
        final Task task = findOneById(userId, id);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final String userId,
                              final Integer index,
                              final String name,
                              final String description) {
        final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task removeByName(final String userId,
                             final String name) {
        final Task task = findOneByName(userId, name);
        if (task == null) return null;
        records.remove(task);
        return task;
    }

    @Override
    public Task changeTaskStatusById(final String userId,
                                     final String id,
                                     final Status status) {
        final Task task = findOneById(userId, id);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(final String userId,
                                        final Integer index,
                                        final Status status) {
        final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

}