package ru.tsc.korosteleva.tm.api.repository;

public interface IAuthRepository {

    void setUserId(String userid);

    String getUserId();

}
