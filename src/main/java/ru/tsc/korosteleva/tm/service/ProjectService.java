package ru.tsc.korosteleva.tm.service;

import ru.tsc.korosteleva.tm.api.repository.IProjectRepository;
import ru.tsc.korosteleva.tm.api.service.IProjectService;
import ru.tsc.korosteleva.tm.enumerated.Status;
import ru.tsc.korosteleva.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.korosteleva.tm.exception.field.*;
import ru.tsc.korosteleva.tm.exception.user.UserIdEmptyException;
import ru.tsc.korosteleva.tm.model.Project;

import java.util.Date;

public class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(final IProjectRepository projectRepository) {
        super(projectRepository);
    }

    @Override
    public Project create(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return repository.create(userId, name);
    }

    @Override
    public Project create(final String userId,
                          final String name,
                          final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return repository.create(userId, name, description);
    }

    @Override
    public Project create(final String userId,
                          final String name,
                          final String description,
                          final Date dateBegin,
                          final Date dateEnd) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        if (dateBegin == null) throw new DateIncorrectException();
        if (dateEnd == null) throw new DateIncorrectException();
        return repository.create(userId, name, description, dateBegin, dateEnd);
    }

    @Override
    public Project findOneByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        Project project = repository.findOneByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @Override
    public Project updateById(final String userId,
                              final String id,
                              final String name,
                              final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        Project project = repository.updateById(userId, id, name, description);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @Override
    public Project updateByIndex(final String userId,
                                 final Integer index,
                                 final String name,
                                 final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index > repository.getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        Project project = repository.updateByIndex(userId, index, name, description);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @Override
    public Project removeByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        Project project = repository.removeByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @Override
    public Project changeProjectStatusById(final String userId,
                                           final String id,
                                           final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        Project project = repository.changeProjectStatusById(userId, id, status);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(final String userId,
                                              final Integer index,
                                              final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index > repository.getSize(userId)) throw new IndexIncorrectException();
        Project project = repository.changeProjectStatusByIndex(userId, index, status);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

}