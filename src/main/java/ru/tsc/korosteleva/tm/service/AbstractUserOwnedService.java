package ru.tsc.korosteleva.tm.service;

import ru.tsc.korosteleva.tm.api.repository.IUserOwnedRepository;
import ru.tsc.korosteleva.tm.api.service.IUserOwnedService;
import ru.tsc.korosteleva.tm.enumerated.Sort;
import ru.tsc.korosteleva.tm.exception.entity.ModelNotFoundException;
import ru.tsc.korosteleva.tm.exception.field.IdEmptyException;
import ru.tsc.korosteleva.tm.exception.field.IndexIncorrectException;
import ru.tsc.korosteleva.tm.exception.user.UserIdEmptyException;
import ru.tsc.korosteleva.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(final R repository) {
        super(repository);
    }

    @Override
    public M add(final String userId, final M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        return repository.add(userId, model);
    }

    @Override
    public List<M> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAll(userId);
    }

    @Override
    public List<M> findAll(final String userId, final Comparator<M> comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll(userId);
        return repository.findAll(userId, comparator);
    }

    @Override
    public List<M> findAll(final String userId, final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        return repository.findAll(userId, sort);
    }

    @Override
    public M findOneById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final M model = repository.findOneById(userId, id);
        if (model == null) throw new ModelNotFoundException();
        return model;
    }

    @Override
    public M findOneByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index > repository.getSize(userId)) throw new IndexIncorrectException();
        M model = repository.findOneByIndex(userId, index);
        if (model == null) throw new ModelNotFoundException();
        return model;
    }

    @Override
    public M remove(final String userId, final M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        return repository.remove(userId, model);
    }

    @Override
    public M removeById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final M model = findOneById(userId, id);
        return remove(model);
    }

    @Override
    public M removeByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index > repository.getSize(userId)) throw new IndexIncorrectException();
        M model = repository.removeByIndex(userId, index);
        if (model == null) throw new ModelNotFoundException();
        return model;
    }

    @Override
    public void clear(final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.clear(userId);
    }

    @Override
    public Integer getSize(final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.getSize(userId);
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(userId, id);
    }

}
