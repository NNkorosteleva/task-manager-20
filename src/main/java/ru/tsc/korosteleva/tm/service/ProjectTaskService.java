package ru.tsc.korosteleva.tm.service;

import ru.tsc.korosteleva.tm.api.repository.IProjectTaskRepository;
import ru.tsc.korosteleva.tm.api.service.IProjectTaskService;
import ru.tsc.korosteleva.tm.exception.field.IdEmptyException;
import ru.tsc.korosteleva.tm.exception.user.UserIdEmptyException;

public class ProjectTaskService implements IProjectTaskService {

    final IProjectTaskRepository projectTaskRepository;

    public ProjectTaskService(final IProjectTaskRepository projectTaskRepository) {
        this.projectTaskRepository = projectTaskRepository;
    }

    @Override
    public void bindTaskToProject(final String userId,
                                  final String projectId,
                                  final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new IdEmptyException();
        projectTaskRepository.bindTaskToProject(userId, projectId, taskId);
    }

    @Override
    public void unbindTaskFromProject(final String userId,
                                      final String projectId,
                                      final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new IdEmptyException();
        projectTaskRepository.unbindTaskFromProject(userId, projectId, taskId);
    }

    @Override
    public void removeProjectById(final String userId, final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        projectTaskRepository.removeProjectById(userId, projectId);
    }
}
