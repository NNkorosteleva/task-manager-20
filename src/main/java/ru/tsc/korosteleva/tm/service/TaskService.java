package ru.tsc.korosteleva.tm.service;

import ru.tsc.korosteleva.tm.api.repository.ITaskRepository;
import ru.tsc.korosteleva.tm.api.service.ITaskService;
import ru.tsc.korosteleva.tm.enumerated.Status;
import ru.tsc.korosteleva.tm.exception.entity.TaskNotFoundException;
import ru.tsc.korosteleva.tm.exception.field.*;
import ru.tsc.korosteleva.tm.exception.user.UserIdEmptyException;
import ru.tsc.korosteleva.tm.model.Task;

import java.util.Collections;
import java.util.Date;
import java.util.List;

public class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(final ITaskRepository repository) {
        super(repository);
    }

    @Override
    public Task create(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return repository.create(userId, name);
    }

    @Override
    public Task create(final String userId,
                       final String name,
                       final String description,
                       final Date dateBegin,
                       final Date dateEnd) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        if (dateBegin == null) throw new DateIncorrectException();
        if (dateEnd == null) throw new DateIncorrectException();
        return repository.create(userId, name, description, dateBegin, dateEnd);
    }

    @Override
    public Task create(final String userId,
                       final String name,
                       final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return repository.create(userId, name, description);
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return repository.findAllByProjectId(userId, projectId);
    }

    @Override
    public Task findOneByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        Task task = repository.findOneByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @Override
    public Task updateById(final String userId,
                           final String id,
                           final String name,
                           final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        Task task = repository.updateById(userId, id, name, description);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @Override
    public Task updateByIndex(final String userId,
                              final Integer index,
                              final String name,
                              final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index > repository.getSize()) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        Task task = repository.updateByIndex(userId, index, name, description);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @Override
    public Task removeByName(final String userId,
                             final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        Task task = repository.removeByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @Override
    public Task changeTaskStatusById(final String userId,
                                     final String id,
                                     final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        Task task = repository.changeTaskStatusById(userId, id, status);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(final String userId,
                                        final Integer index,
                                        final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index > repository.getSize()) throw new IndexIncorrectException();
        Task task = repository.changeTaskStatusByIndex(userId, index, status);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

}